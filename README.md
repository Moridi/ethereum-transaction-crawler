# Ethereum Internal Transaction Crawler

Ethereum Internal Transaction Crawler lets you extract 
internal transactions from [etherscan.io](https://etherscan.io/) and
transform them into CSV format.

## Quick Start

### Install Requirements

```bash
sudo apt-get install libcurl4-openssl-dev
sudo apt-get install libcurlpp-dev
sudo apt-get install cmake
```

### Build
```bash
mkdir build
cd build
cmake ..
make
```

### Run

```bash
./etherscan-crawler <start_block> <end_block> <base_directory>
```

### Example

The following command will extract all the internal transactions 
between the blocks with number of *1000000* and *1500000* and collect them into 
*internal_dir* as multiple CSV files.

```bash
./etherscan-crawler 1000000 1500000 internal_dir
```