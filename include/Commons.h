constexpr int RED_COLOR_INDEX = 1;
constexpr int GREEN_COLOR_INDEX = 2;
constexpr int RESET_COLOR_INDEX = 4;

char COLOR_CODES[5][255] =
{
	"\033[1m\033[30m",		// Bold BLACK
	"\033[1m\033[31m",		// Bold RED
	"\033[1m\033[32m",		// Bold GREEN
	"\033[1m\033[34m",		// Bold BLUE
	"\x1B[0m"				// Reset
};