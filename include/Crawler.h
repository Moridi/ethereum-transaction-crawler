#ifndef CRAWLER_H_
#define CRAWLER_H_

#include <memory>
#include <string>

#include <json/json.h>

constexpr int NO_THREAD = 20;

class Crawler
{
public:
    struct ThreadData
    {
        inline ThreadData(int from_, int to_,
                int thread_id_, std::string file_path_);

        int from;
        int to;
        int thread_id;
        std::string file_path;
    };
    
    static inline std::shared_ptr<Crawler> get_instance();
    
    static void* thread_crawler(void* data);
    static inline std::string get_internal_transactions(int from, int to, int page);
    inline std::string get_block_number_by_time(int time_stamp);

    void crawl(int from, int to, std::string directory);

private:
    inline Crawler();
    static std::shared_ptr<Crawler> instance;
    static inline Json::Value crawl_single_page(int from, int to, int page);
    static inline std::string get_file_path(std::string directory, int thread_id);
    inline std::string create_directory(int from, int to, std::string directory);
};

typedef std::shared_ptr<Crawler> CrawlerSharedPointer;
#include "Crawler-inl.h"

#endif