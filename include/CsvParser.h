#ifndef CSV_PARSER_H_
#define CSV_PARSER_H_

#include <string>

#include <json/json.h>

class CsvParser
{
public:
    inline static void create_base_file(std::string file_name);
    inline static void write_json_value(std::string file_name,
            Json::Value json_value);
};

#include "CsvParser-inl.h"

#endif