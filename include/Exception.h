#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <exception>
#include <string>

class NullValueException : public std::exception
{
public:
    inline NullValueException(std::string message_);
    inline const char* what() const throw();
private:
    std::string message;
};

#include "Exception-inl.h"

#endif