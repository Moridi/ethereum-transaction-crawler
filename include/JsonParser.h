#ifndef JSON_PARSER_H_
#define JSON_PARSER_H_

#include <json/json.h>

class JsonParser
{
public:
    inline static Json::Value parse(std::string text);
    inline static bool is_last_page(Json::Value json_result);
    inline static bool is_invalid_value(Json::Value json_result);
};

#include "JsonParser-inl.h"

#endif