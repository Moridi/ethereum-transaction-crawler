#ifndef STRING_UTILS_H_
#define STRING_UTILS_H_

class StringUtils
{
public:
    static inline int get_int(char* text);
};

#include "StringUtils-inl.h"

#endif