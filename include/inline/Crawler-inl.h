#ifndef CRAWLER_INL_H_
#define CRAWLER_INL_H_

#ifndef CRAWLER_H_
#error "Crawler-inl.h" should be included only in "Crawler.h" file.
#endif

#include "Crawler.h"

#include <sys/stat.h>
#include <sys/types.h>

#include <string>
#include <sstream>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>

#include "JsonParser.h"
#include "Exception.h"

constexpr char API_TOKEN[] = "54QSR3BGEME9G219KBKVTVSZMIICU6AEMG";
constexpr char API_SERVICE_ADDRESS[] = "https://api.etherscan.io/api";
constexpr int MAX_TX_PER_REQUEST = 10000;

Crawler::Crawler()
{
    curlpp::Cleanup myCleanup;
}

CrawlerSharedPointer Crawler::get_instance()
{
    if (instance == nullptr)
		instance = std::make_shared<Crawler>(Crawler());
	return instance;
}

std::string Crawler::get_block_number_by_time(int time_stamp)
{
    std::ostringstream os;

    os << curlpp::options::Url(
            std::string(API_SERVICE_ADDRESS) +
            "?module=block" +
            "&action=getblocknobytime" +
            "&timestamp=" + std::to_string(time_stamp) +
            "&closest=before" +
            "&apikey=" + API_TOKEN);

    return os.str();
}

std::string Crawler::get_internal_transactions(int from, int to, int page)
{
    std::ostringstream os;
    
    os << curlpp::options::Url(
        std::string(API_SERVICE_ADDRESS) +
        "?module=account" +
        "&action=txlistinternal" +
        "&startblock=" + std::to_string(from) +
        "&endblock=" + std::to_string(to) +
        "&page=" + std::to_string(page) +
        "&offset=" + std::to_string(MAX_TX_PER_REQUEST) +
        "&sort=asc" +
        "&apikey=" + API_TOKEN);
        
    return os.str();
}

Json::Value Crawler::crawl_single_page(int from, int to, int page)
{
    std::string page_str = get_internal_transactions(from, to, page);
    Json::Value page_json = JsonParser::parse(page_str);
    if (JsonParser::is_invalid_value(page_json["result"]))
        throw NullValueException(page_json["message"].asString());
    return page_json["result"];
}

Crawler::ThreadData::ThreadData(int from_, int to_,
        int thread_id_, std::string file_path_)
: from(from_),
to (to_),
thread_id(thread_id_),
file_path(file_path_)
{
}

std::string Crawler::create_directory(int from, int to, std::string directory)
{
    mkdir(directory.c_str(), 0777);

    std::string directory_name = directory + "/" +
            std::to_string(from) + "-" +
            std::to_string(to) + "/";
    mkdir(directory_name.c_str(), 0777);

    return directory_name;
}

std::string Crawler::get_file_path(std::string directory, int thread_id)
{
    return directory + std::to_string(thread_id);
}

#endif