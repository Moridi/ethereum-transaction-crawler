#ifndef CSV_PARSER_INL_H_
#define CSV_PARSER_INL_H_

#ifndef CSV_PARSER_H_
#error "CsvParser-inl.h" should be included only in "CsvParser.h" file.
#endif

#include "CsvParser.h"

#include "fstream"

constexpr int NUMBER_OF_COLUMNS = 14;
constexpr char COLUMNS[NUMBER_OF_COLUMNS][20] {
    "blockNumber",
    "timeStamp",
    "hash",
    "from",
    "to",
    "value",
    "contractAddress",
    "input",
    "type",
    "gas",
    "gasUsed",
    "traceId",
    "isError",
    "errCode",
};

void CsvParser::create_base_file(std::string file_name)
{
    std::fstream fout;
    fout.open(file_name + ".csv", std::ios::out); 

    for (uint8_t i = 0; i < NUMBER_OF_COLUMNS - 1; i++)
        fout << COLUMNS[i] << ",";
        
    fout << COLUMNS[NUMBER_OF_COLUMNS - 1] << "\n";
}

void CsvParser::write_json_value(std::string file_name,
        Json::Value json_value)
{
    std::fstream fout;
    fout.open(file_name + ".csv", std::ios::app); 

    for (int i = 0; i < json_value.size(); i++)
    {
        for (int j = 0; j < NUMBER_OF_COLUMNS - 1; j++)
            fout << json_value.get(i, -1)[COLUMNS[j]] << ",";
        
        fout << json_value.get(i, -1)[COLUMNS[NUMBER_OF_COLUMNS - 1]] << "\n";
    }

    fout.close();
}

#endif