#ifndef EXCEPTION_INL_H_
#define EXCEPTION_INL_H_

#ifndef EXCEPTION_H_
#error "Exception-inl.h" should be included only in "Exception.h" file.
#endif

#include "Exception.h"

NullValueException::NullValueException(std::string message_)
: message(message_)
{
}

const char* NullValueException::what() const throw()
{
    return message.c_str();
}

#endif