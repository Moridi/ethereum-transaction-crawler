#ifndef JSON_PARSER_INL_H_
#define JSON_PARSER_INL_H_

#ifndef JSON_PARSER_H_
#error "JsonParser-inl.h" should be included only in "JsonParser.h" file.
#endif

#include <iostream>

#include "JsonParser.h"

constexpr int JSON_VALUE_ARRAY_LIKE = 6;

Json::Value JsonParser::parse(std::string text)
{
    Json::Value root;
    Json::Reader reader;
    
    bool parsingSuccessful = reader.parse(text, root);
    
    if (!parsingSuccessful)
        std::cerr << "Error parsing the string" << std::endl;

    return root;
}

bool JsonParser::is_last_page(Json::Value json_result)
{
    std::string last_page = "[]";

    Json::Value last_page_json;
    Json::Reader reader;
    
    reader.parse(last_page, last_page_json);

    return json_result == last_page_json;
}

bool JsonParser::is_invalid_value(Json::Value json_result)
{
    return json_result.type() != JSON_VALUE_ARRAY_LIKE;
}

#endif