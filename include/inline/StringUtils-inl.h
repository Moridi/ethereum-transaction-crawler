#ifndef STRING_UTILS_INL_H_
#define STRING_UTILS_INL_H_

#ifndef STRING_UTILS_H_
#error "StringUtils-inl.h" should be included only in "StringUtils.h" file.
#endif

#include "StringUtils.h"

#include <sstream>

int StringUtils::get_int(char* text)
{
    int number;
    std::stringstream sstream(text);
    sstream >> number;
    return number;
}

#endif