#include "Crawler.h"

#include <time.h>
#include <pthread.h>

#include <chrono>

#include "JsonParser.h"
#include "CsvParser.h"
#include "Commons.h"

CrawlerSharedPointer Crawler::instance;

using namespace std;

void print_valid_writing(int page)
{
    char result[1024];

    sprintf(result, "%sPage number: %d%s\n",
            COLOR_CODES[GREEN_COLOR_INDEX],
            page,
            COLOR_CODES[RESET_COLOR_INDEX]);
    
    printf("%s", result);
}

void print_error(int page, const exception& e)
{
    char result[1024];

    sprintf(result, "%sPage number: %d\nError message:\n\t%s%s\n",
            COLOR_CODES[RED_COLOR_INDEX],
            page, e.what(),
            COLOR_CODES[RESET_COLOR_INDEX]);

    printf("%s", result);
}

void* Crawler::thread_crawler(void* data)
{
    ThreadData* thread_data = (ThreadData*)data;
    int from = thread_data->from;
    int to = thread_data->to;
    int thread_id = thread_data->thread_id;
    string file_path = thread_data->file_path;

    CsvParser::create_base_file(file_path);

    int page = thread_id + 1;

    bool is_last_page = false;

    while (!is_last_page)
    {
        try
        {
            Json::Value single_page = crawl_single_page(from, to, page);
            is_last_page = JsonParser::is_last_page(single_page);
            CsvParser::write_json_value(file_path, single_page);
            print_valid_writing(page);
            page += NO_THREAD;
        }
        catch(const exception& e)
        {
            print_error(page, e);
        }
    }

    pthread_exit(NULL);
}

void Crawler::crawl(int from, int to, string base_directory)
{
    auto start = std::chrono::high_resolution_clock::now();

    pthread_t crawlers[NO_THREAD];
    ThreadData* thread_data[NO_THREAD];
    string directory = create_directory(from, to, base_directory);

    for (int i = 0; i < NO_THREAD; i++)
    {
        string file_path = get_file_path(directory, i);
        thread_data[i] = new Crawler::ThreadData(from, to, i, file_path);
    }

    for (int i = 0; i < NO_THREAD; i++)
        pthread_create(&crawlers[i], NULL,
                thread_crawler, (void*)thread_data[i]);

    for (int i = 0; i < NO_THREAD; i++)
        pthread_join(crawlers[i], NULL);

    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Total time: " << elapsed.count() << " s\n";
}