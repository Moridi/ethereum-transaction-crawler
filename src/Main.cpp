#include <iostream>

#include "Crawler.h"
#include "StringUtils.h"

int main(int argc, char* argv[])
{
    CrawlerSharedPointer crawler = Crawler::get_instance();

    if (argc < 3)
        std::cerr << "./etherscan-crawler <from> <to>" << std::endl;
    else
        crawler->crawl(StringUtils::get_int(argv[1]),
                StringUtils::get_int(argv[2]), argv[3]);
        
    return 0;
}